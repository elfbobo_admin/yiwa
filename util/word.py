# coding: utf-8

"""自定义分词处理，基于jieba分词"""
import re
import jieba
import jieba.analyse as al
import platform
import os

def removePunctuation(text):
    """只保留中文、大小写字母和阿拉伯数字"""
    if not text or not isinstance(text, str):
        return ""
    reg = "[^0-9A-Za-z\u4e00-\u9fa5]"
    return re.sub(reg, '', text)


def cut(text, loadDict=False, stopWord=False, toStr=False):
    """分词"""

    # linux开启并行计算
    if platform.system() != "Windows" and jieba.pool is None:
        jieba.enable_parallel(os.cpu_count())

    # 加载自定义字典分词
    if loadDict:
        jieba.load_userdict(r"./user_dictionary.txt")

    # 关键词分词提取
    if stopWord:
        # 加载停用词
        for fname in ("baidu", "hit", "scu"):
            al.set_stop_words(rf"./stopwords/{fname}.txt")
        words = al.extract_tags(text, 100)
        return words if not toStr else ",".join(words)

    # 只保留中文、大小写字母和阿拉伯数字
    text = removePunctuation(text)
    # 不去停用词
    words = jieba.lcut(text)
    return words if not toStr else ",".join(words)
