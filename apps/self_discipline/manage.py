# coding: utf8

"""自律表格管理页面
二维码管理, 不支持语音指令.
"""

import datetime
from flask_restful import Resource, reqparse
from apps import api
from apps.self_discipline.db import s3db
from yiwa.upload import FileUploader
from apps.self_discipline.configs import ImageRelPath
from util.date_time import DateTimeUtil
from apps.__public._memcache import Memcache
from apps.__public._db import refresh_command

parser = reqparse.RequestParser()


# 维护兑换记录
class ExchangeRecords(Resource):
    """兑换记录"""

    def get(self):
        """查看兑换记录"""
        sql = f"""select award, number, `time`
            from exchange_records 
            where last=0
            order by time desc;
        """
        data = s3db.select(sql, as_dict=True)
        return data

    def post(self):
        """兑换奖品，添加兑换记录"""
        parser.add_argument("award_id", type=int, location='form')
        args = parser.parse_args()
        award_id = args.get("award_id")
        sql = f"select * from awards where id={award_id} and got=0;"
        award = s3db.select_one(sql)
        # 余额
        sql = f"""select balance from exchange_records where last=1;"""
        balance = s3db.select_one(sql)
        if award and balance and balance.get("balance") >= award.get("score"):
            if self._exchange(award):
                return {"code": 1, "msg": "兑换成功"}
        return {"code": 0, "msg": "兑换失败或余额不足"}

    def _exchange(self, award_dict):
        """兑换"""
        award_id = award_dict.get("id")
        award = award_dict.get("name")
        number = award_dict.get("score")
        time = datetime.datetime.now()
        # 插入一条兑换记录
        sql = f"""insert into exchange_records(last, award, number, `time`) 
            values(0, '{award}', '{number}', '{time}');"""
        # 余额减少
        sql = f"""{sql}
            update exchange_records set balance=balance-{number}
            where last=1;
        """
        # 获取奖品，更新兑换时间
        sql = f"""{sql}
            update awards set got=1, got_time='{time}' where id={award_id};
        """
        result = s3db.execute(sql)
        return result


api.add_resource(ExchangeRecords, "/self_discipline/exchange_records")


# 维护奖品
class Award(Resource):
    """奖品"""

    def get(self, award_id):
        """某项"""
        sql = f"select * from awards where id={award_id};"
        data = s3db.select_one(sql)
        return data

    def put(self, award_id):
        """更新某项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("score", type=int, location='form')
        parser.add_argument("image_data", type=str, location='form')
        args = parser.parse_args()
        name = args.get("name")
        score = args.get("score")
        image_data = args.get("image_data")
        file_name = FileUploader().image(ImageRelPath, image_data)
        image_path = f"image/{file_name}"
        sql = f"""update awards 
            set name='{name}', score='{score}', image_path='{image_path}' 
            where id={award_id};
        """
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}

    def delete(self, award_id):
        """逻辑删除某项"""
        sql = f"update awards set enable=0 where id={award_id};"
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}


class Awards(Resource):
    """奖品列表"""

    def get(self):
        """返回未领取的奖品"""
        sql = f"select * from awards where got=0 and enable=1 order by score;"
        data = s3db.select(sql, as_dict=True)
        return data

    def post(self):
        """添加奖品"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("score", type=int, location='form')
        parser.add_argument("image_data", type=str, location='form')
        args = parser.parse_args()
        name = args.get("name")
        score = args.get("score")
        image_data = args.get("image_data")
        file_name = FileUploader().image(ImageRelPath, image_data)
        image_path = f"image/{file_name}"
        sql = f"""insert into awards(name, score, image_path, got) 
            values('{name}', '{score}', '{image_path}', 0);"""
        last_id = s3db.execute(sql, result_set=True)
        if last_id:
            return {"code": 1, "msg": last_id}
        return {"code": 0, "msg": 0}


api.add_resource(Award, "/self_discipline/awards/<int:award_id>")
api.add_resource(Awards, "/self_discipline/awards")


# 维护分值鼓励项
class Score(Resource):
    """分值项"""

    def get(self, score_id):
        """某个分值项"""
        sql = f"select * from scores where id={score_id} and enable=1;"
        data = s3db.select_one(sql)
        return data

    def put(self, score_id):
        """更新某项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("image_data", type=str, location='form')
        args = parser.parse_args()
        name = args.get("name")
        image_data = args.get("image_data")
        file_name = FileUploader().image(ImageRelPath, image_data)
        image_path = f"image/{file_name}"
        sql = f"""update scores 
            set name='{name}', image_path='{image_path}' 
            where id={score_id};
        """
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}

    def delete(self, score_id):
        """逻辑删除某项"""
        sql = f"update scores set enable=0 where id={score_id};"
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}


class Scores(Resource):
    """分值列表"""

    def get(self):
        """返回列表"""
        sql = f"select * from scores where enable=1;"
        data = s3db.select(sql, as_dict=True)
        return data

    def post(self):
        """添加分值项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("image_data", type=str, location='form')
        args = parser.parse_args()
        name = args.get("name")
        image_data = args.get("image_data")
        file_name = FileUploader().image(ImageRelPath, image_data)
        image_path = f"image/{file_name}"
        # 所有分值都是1
        sql = f"""insert into scores(score, name, image_path) 
            values(1, '{name}', '{image_path}');"""
        last_id = s3db.execute(sql, result_set=True)
        if last_id:
            return {"code": 1, "msg": last_id}
        return {"code": 0, "msg": 0}


api.add_resource(Score, "/self_discipline/scores/<int:score_id>")
api.add_resource(Scores, "/self_discipline/scores")


# 维护每日计分项
class Options(Resource):
    """每日计分项列表"""

    def get(self):
        """返回列表"""
        sql = f"select * from options where enable=1 order by `order`;"
        data = s3db.select(sql, as_dict=True)
        return data

    def post(self):
        """添加计分项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("order", type=int, location='form')
        parser.add_argument("image_data", type=str, location='form')
        args = parser.parse_args()
        name = args.get("name")
        order = args.get("order")
        image_data = args.get("image_data")
        file_name = FileUploader().image(ImageRelPath, image_data)
        image_path = f"image/{file_name}"
        sql = f"""insert into options(name, `order`, image_path) 
            values('{name}', '{order}', '{image_path}');"""
        last_id = s3db.execute(sql, result_set=True)
        if last_id:
            refresh_command()
            Memcache().refresh(1)
            return {"code": 1, "msg": last_id}
        return {"code": 0, "msg": 0}


class Option(Resource):
    """每日计分项"""

    def get(self, option_id):
        """某项"""
        sql = f"select * from options where id={option_id};"
        data = s3db.select_one(sql)
        return data

    def put(self, option_id):
        """更新某项"""
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("order", type=int, location='form')
        parser.add_argument("image_data", type=str, location='form')
        args = parser.parse_args()
        name = args.get("name")
        order = args.get("order")
        image_data = args.get("image_data")
        file_name = FileUploader().image(ImageRelPath, image_data)
        image_path = f"image/{file_name}"
        sql = f"""update options 
            set name='{name}', `order`='{order}', image_path='{image_path}' 
            where id={option_id};
        """
        if s3db.execute(sql):
            refresh_command()
            Memcache().refresh(1)
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}

    def delete(self, option_id):
        """逻辑删除某项"""
        sql = f"update options set enable=0 where id={option_id};"
        if s3db.execute(sql):
            refresh_command()
            Memcache().refresh(1)
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}


api.add_resource(Option, "/self_discipline/options/<int:option_id>")
api.add_resource(Options, "/self_discipline/options")


# 维护宝贝信息
class Baby(Resource):
    """宝贝"""

    def get(self, baby_id):
        """某项"""

        def _baby():
            sql = f"select * from baby where enable=1 limit 1;"
            return s3db.select_one(sql)

        data = _baby()
        if not data:
            sql = f"""
                insert into baby(name,birthday,enable)
                values("宝宝",'{DateTimeUtil.now.date()}',1)
                """
            s3db.execute(sql)
            data = _baby()
        return data

    def put(self, baby_id):
        parser.add_argument("name", type=str, location='form')
        parser.add_argument("birthday", type=str, location='form')
        args = parser.parse_args()
        name = args.get("name")
        birthday = args.get("birthday")
        sql = f"""update baby set name='{name}', birthday='{birthday}' 
            where id={baby_id};"""
        if s3db.execute(sql):
            return {"code": 1, "msg": True}
        return {"code": 0, "msg": False}


api.add_resource(Baby, "/self_discipline/baby/<int:baby_id>")
