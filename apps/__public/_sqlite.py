# coding: utf8

"""
falsk操作sqlite数据库
参考https://dormousehole.readthedocs.io/en/latest/patterns/sqlite3.html
"""
from apps import app
import sqlite3
from flask import g
from yiwa.log import Log
import os
from yiwa.settings import BASE_DIR

logger = Log().logger
DATABASE = os.path.join(BASE_DIR, 'yiwa.s3db')


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE, timeout=10)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def make_dicts(cursor, row):
    return dict((cursor.description[idx][0], value)
                for idx, value in enumerate(row))


def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv


def select(sql):
    with app.app_context():
        db = get_db()
        db.row_factory = make_dicts
        try:
            return query_db(sql)
        except sqlite3.OperationalError as oe:
            logger.error(f"sqlite3查询数据报错 {oe}")
            return []
