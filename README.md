# YiWa

#### 介绍

- 伊瓦（伊娃&瓦力），用于家庭数据展示和简单交互（例如：儿童自律表，幼儿园算数，随机爬取一个恐龙图片等）；
    需要一个树莓派（网络、麦克风）和一台电视机（HDMI线）无需鼠标，只要说出指令，页面跳转到对应功能；
    它是一个伪AI项目，全部基于百度AI开放平台，语音识别指令，NLP语义理解；可以理解为带有大屏的假"智能音箱"。
- 支持Ubuntu, Deepin, Fedora, Mac等类Unix系统下调试开发运行, 不支持Windows.


#### 截图

- 语音状态

![语音状态](https://images.gitee.com/uploads/images/2021/0203/225429_25798f4a_25066.jpeg "QQ20210203-225408@2x.jpg")

- 指令目录

![指令目录](https://images.gitee.com/uploads/images/2019/0705/115759_f9f4be9f_25066.png "WX20190705-114647-指令目录.png")

- 自律表格（扫描二维码在手机端管理，在家庭无线网内使用）

![自律表格](https://images.gitee.com/uploads/images/2021/0203/225451_c6319cab_25066.jpeg "QQ20210203-225341@2x.jpg")
![自律表格管理界面](https://images.gitee.com/uploads/images/2020/0806/161536_82f5e3c9_25066.jpeg "自律表格管理界面.jpg")

#### 树莓派运行时样子，有视频，树莓派2b+usb麦克风+usb无线网卡
 :movie_camera:  :point_right: [操作录像已上传B站，点我去看](https://www.bilibili.com/video/BV1t5411n7Sv) :point_left: 

![树莓派](https://images.gitee.com/uploads/images/2021/0203/225542_07111d31_25066.jpeg "QQ20210203-225132@2x.jpg")
- 抖音上育子心得，不少家长建议少看电视，觉得有道理，那就把它改为语音交互的视觉输出端！


#### 大概流程

接收麦克风语音，本地离线匹配唤醒词；唤醒后继续接收麦克风指令，指令接入百度语音识别平台，转换成文字；
接着继续使用百度AI平台的NLP功能，将指令短文本和本地预设好的指令短文本，做语义比较；
匹配成功后，通过selenium打开本地预设指令对应的url地址，就此实现无鼠标语音控制页面内容展示功能。


#### 安装教程，[看Wiki](https://gitee.com/bobo2cj/yiwa/wikis)

1. pip install -r requirements.txt -i http://mirrors.aliyun.com/pypi/simple/ --trusted-host mirrors.aliyun.com
2. selenium使用的谷歌驱动文件需要替换为你本地chrome对应的版本，当前chrome84版本，
    下载地址：[这里选择不同版本](https://chromedriver.storage.googleapis.com/index.html)
3. 下载好chromedriver文件后不要更改文件名，就保持chromedriver或chromedriver.exe；
4. 替换自己的百度AI的key（[申请指南](https://ai.baidu.com/docs#/Begin/top)），
    配置分别在asr/configs.py和nlp/configs.py文件修改（百度里分属两个不同的应用）。
5. 注意snowboy的安装(建议找下载速度较快的gitee搬运仓库), 
    不同系统平台不一样(这里已经编译了苹果和树莓派). [参考这里](https://www.jianshu.com/p/a1c06020f5fd)

#### 使用说明（程序启动），[看Wiki](https://gitee.com/bobo2cj/yiwa/wikis)

1. python3.6 web.py
2. python3.6 yiwa.py
3. 启动无误后，即可对着你的麦克风说话，目前支持指令：返回首页，刷新指令，显示所有指令，重启等；


#### 本地开发，[看Wiki](https://gitee.com/bobo2cj/yiwa/wikis)

0. 最好基于类Linux系统开发（Debian，Ubuntu，Fedora，MacOS）
1. 可开发基于Flask的插件页面，插件以独包形式，放入apps文件夹下，
    插件必需包含configs.py文件，并指定必需的参数:APPID :str, APPNAME :str, COMMANDS :dict
2. 插件的__init__.py文件必需导入其他flask页面文件，参考已存在的插件包。
3. 插件包的静态文件放入各自包里
4. 约定：
    - 插件必需以单独文件夹形式放入apps文件夹中
    - 插件文件夹根目录，必需包含自我介绍的配置文件，configs.py文件，至少有：appid，appname，

#### 注意

- ！！！请申请你自己的百度AI应用key，方法在下面（安装教程）！！！
- 有个别不换自己的百度key, 把我的日pv数量用光, 强烈谴责.
- 本人水平有限，能力一般，项目难度不大，你也可以。
- 本地部署运行文档见[项目Wiki](https://gitee.com/bobo2cj/yiwa/wikis)。
    
#### TODO
-  :white_check_mark: 做几个页面（至少包含欢迎和指令目录）
-  :white_check_mark: 使用websocket刷新yiwa的唤醒状态和语音指令，下一步加入是否“正在录音”的界面标识，
    方便用户知道什么时候该说出指令，而不是去“碰”。
-  :white_check_mark: 提升下语音指令匹配的速度，先采用本地词库来预匹配，找到对应插件，再接百度，
    缩小遍历范围（本地词库需要去生产词库文件，如果采用自动化去生成，对于速度没有保障，
    故而采用[jieba分词](https://github.com/fxsjy/jieba)处理）
-  :white_check_mark: 做下tts文本转语音基础功能，供插件需要发出语音响应时调用（缺少对不同平台的处理，
    例如mac用mei-jia，linux用xxx，windows用xxx）
-  :white_check_mark: 做个二维码生成基础功能，供插件调用，有的插件可能需要借助手机来配置，例如闹钟，通勤地点等
    (渲染时需要使用自定义的render方法，看home页面的实现)
-  :white_check_mark: 做个系统配置管理页，修改一些能带来不同体验的参数，录音间隔时间和录音时长。
-  :white_check_mark: ip地址动态获取局域网本地ip
-  :white_check_mark: 自律表格（完成一项加一个鼓励，以鼓励为主，没有惩罚；后台可以配置自律项和鼓励项，以及奖励，兑换功能等；）
- 幼儿园算术题目（10以内加减法，可后台配置计算范围、加减法等）
- 汉字笔顺动图展示
- 随机出一张喜欢的图，照着画画（我家孩子喜欢恐龙和工程车，后台可自行配置关键词）
-  :white_check_mark: 修改指令匹配逻辑，一级指令下只能唤醒该功能下的语音指令，避免功能间指令冲突
-  :white_check_mark: 修改顶部状态栏录音状态样式，录音中（绿色圆声音波段动图），识别中（黄色圆转圜动图），失败（红色圆感叹号图）
- 英语单词
- 闹钟、上学提醒等
-  :white_check_mark: 停下来优化调整（部分代码替换为python的特性写法）
- 做个在线升级和安装apps的功能，备份本地apps，从gitee拉取代码，替换apps，重启服务。
- 高德地图通勤（跨省/市）
- 做菜倒计时
- 出门检查项
- 中国历史编年线
- 中国古诗词诗人所在年代line
- 贪吃蛇（语音控制：前后左右，调头[近食物]，停下[按时缩短]，加速，跳过，隐身，自动；设置：食物个数，食物种类和增长点，穿墙，穿过自身，障碍物，老鹰飞过等；其他：长度排名[时间点|长度]）
- 会优先做一个猜水果游戏[苹果、茄子、双星等]，在家里和孩子一起玩。
- 等
- 后面的后面想到再加，土比亢踢牛的。。。