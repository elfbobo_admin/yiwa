# coding: utf8

"""基于snowboy唤醒词
参考：http://docs.kitt.ai/snowboy/
"""
import signal
from yiwa.db import DataConveyor
from asr.configs import MODEL_FILE
from snowboy import snowboydecoder

interrupted = False


def signal_handler(signal, frame):
    global interrupted
    interrupted = True


def interrupt_callback():
    global interrupted
    return interrupted


def wakeup():
    global interrupted
    interrupted = True
    snowboydecoder.play_audio_file()


def wakeup_waiting():
    """唤醒等待"""
    global interrupted
    # capture SIGINT signal, e.g., Ctrl+C
    signal.signal(signal.SIGINT, signal_handler)
    detector = snowboydecoder.HotwordDetector(MODEL_FILE, sensitivity=0.5)
    data_conveyor = DataConveyor()
    print("Start Recording...")
    data_conveyor.listening("待唤醒")
    # main loop
    detector.start(detected_callback=wakeup,
                   interrupt_check=interrupt_callback,
                   sleep_time=0.03)
    detector.terminate()
    print("Recording Done...")
    data_conveyor.listened("已唤醒")
    detector.ring_buffer._buf.clear()   # 清理唤醒的语音流缓冲
    interrupted = False
    return True


if __name__ == "__main__":
    print(wakeup_waiting())
